<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	
    /**
     * @var array
     */
    protected $fillable = [
	  'name'
	];

}
