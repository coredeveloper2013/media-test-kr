<?php

namespace App\Http\Controllers;

use App\Model\Image;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Resources\ImageResource;
use App\Http\Resources\ImageCollection;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return Image::all();

         return new ImageCollection(Image::all());

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try {

            $inputs = $request->all();
            $dir='upload/images';
            $current_timestamp = Carbon::now()->timestamp;
            $image = 'im_' . $current_timestamp . '.' . $request->file('name')->getClientOriginalExtension();
            $request->file('name')->storeAs($dir, $image);
            $inputs['name'] = $image;
            $image_name=Image::create($inputs);

            return response()->json([
                'message' => 'Created successfully ',
                'data' => $image_name,
            ], 201);


            //Flash::success('Ad created successfully.');
        } catch (\Exception $exception) {

            return response()->json([
                    'message' => $exception->getMessage(),
                ], 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
         return new ImageResource($image);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {

         try {

            if ($image->id) {
                $dirs='upload/images/';
                Storage::delete($dirs.$image->name);
            }
            $image->delete();
            return response()->json([
                'message' => 'Deleted successfully',
                'data' => $image->id,
            ], 200);

        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage(),
            ], 500);
        }
    }
}
